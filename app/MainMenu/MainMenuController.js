'use strict';

angular
    .module('myApp')
    .controller('MainMenuController', function($scope) {
        $scope.mainMenu = [
            { 'name': 'Покемоны', 'url': 'list' },
            { 'name': 'Создать нового', 'url': 'createNewPokemon' },
            { 'name': 'Личный кабинет', 'url': 'myaccount' }
            ];
      }
    );
