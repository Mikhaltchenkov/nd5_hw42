'use strict';

angular
    .module('myApp')
    .controller('MyAccountCtrl', function ($scope, AccountService) {
        var vm = this;
        vm.user = AccountService.getUserInfo();

        vm.registeredUser = (vm.user.name) ? true : false;

        vm.register = function (user) {
            AccountService.saveUserInfo(user);
            vm.registeredUser = true;
        };

    });
