'use strict';

angular
    .module('myApp')
    .factory('AccountService', function() {

        const state = {
            userInfo: {}
        };

        return {
            getUserInfo()  {
                return state.userInfo;
            },
            saveUserInfo(item) {
                state.userInfo = {
                    name: item.name,
                    email: item.email,
                    phone: item.phone
                };
            }
        };

    })

