'use strict';

var MyAccountPage = function () {
    var userNameInput = element(by.model('vm.user.name'));
    var userEmailInput = element(by.model('vm.user.email'));

    this.get = function () {
        browser.get('/#!/myaccount');
    };

    this.getNameInputClassList = function () {
        return userNameInput.getAttribute('class');
    };

    this.getEmailInputClassList = function () {
        return userEmailInput.getAttribute('class');
    };

    this.setName = function (name) {
        userNameInput.sendKeys(name);
    };

    this.setEmail = function (email) {
        userEmailInput.sendKeys(email);
    };
    
};

module.exports = MyAccountPage;
