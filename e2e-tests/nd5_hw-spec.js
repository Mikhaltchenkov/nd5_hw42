'use strict';

const expect = require('chai').expect;

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('Pokemon Shop', function () {

    it('Заголовок страницы должен быть "My AngularJS App"', function () {
        browser.get('/');
        let title = browser.getTitle();
        expect(title).eventually.to.equal('My AngularJS App');
    });

    it('Подсветка текущего пункта меню', function () {
        browser.get('/#!/list');
        expect(element(by.id('menu-list')).getAttribute('class')).eventually.to.contain('btn-primary');
    });

    it('По кнопке Add покемон помещается в корзину', function () {
        let list = element.all(by.repeater('singlePokemon in vm.pokemons | filter:vm.myQuery | orderBy: vm.myOrderProperty'));
        list.first().element(by.buttonText('Add')).click();
        let cart = element.all(by.repeater('(singlePokemonIndex, singlePokemonValue) in $ctrl.cartItems'));
        expect(cart.count()).eventually.to.be.equal(1);
    });


});
describe('Личный кабинет', function () {

    let MyAccountPage = require('./MyAccount-po.js');

    let MyAccount = new MyAccountPage();

    it('По адресу /myaccount открывается форма редактирования личных данных', function () {
        MyAccount.get();
        expect(element(by.id('myaccount')).getText()).eventually.to.equal('Личный кабинет');
    });

    it('Поле "Имя" обязательное', function () {
        expect(MyAccount.getNameInputClassList()).eventually.to.contain('ng-invalid-required');
    });

    it('Поле "E-Mail" обязательное', function () {
        expect(MyAccount.getEmailInputClassList()).eventually.to.contain('ng-invalid-required');
    });

    it('Поле "E-Mail" не принимает не корректные адреса', function () {
        MyAccount.setEmail('dima@dm-dev');
        expect(MyAccount.getEmailInputClassList()).eventually.to.contain('ng-invalid');
    });

    it('Поле "E-Mail" принимает правильные адреса', function () {
        MyAccount.setEmail('.ru');
        expect(MyAccount.getEmailInputClassList()).eventually.to.contain('ng-valid');
    });

});


